package com.pattarapol.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldWithdrawSuccess(){
        BookBank book = new BookBank("Pattarapol", 100.0);
        book.withdraw(50);
        assertEquals(50, book.getBalance(),0.00001);
    }

    @Test
    public void shouldWithdrawOverBalance(){
        BookBank book = new BookBank("Pattarapol", 100.0);
        book.withdraw(150);
        assertEquals(100, book.getBalance(),0.00001);
    }

    @Test
    public void shouldWithdrawNegativeNumber(){
        BookBank book = new BookBank("Pattarapol", 100.0);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(),0.00001);
    }
    
    @Test
    public void shouldDepositSuccess(){
        BookBank book = new BookBank("Pattarapol", 100.0);
        book.deposit(100);
        assertEquals(200, book.getBalance(),0.00001);
    }

    
    @Test
    public void shouldDepositNegativeNumber(){
        BookBank book = new BookBank("Pattarapol", 100.0);
        book.deposit(-50);
        assertEquals(100, book.getBalance(),0.00001);
    }

    

}
