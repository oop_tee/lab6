package com.pattarapol.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals("Robot", robot.getName());
        assertEquals('R',robot.getSymbol());
        assertEquals(10,robot.getX());
        assertEquals(11,robot.getY());

    }
    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R',robot.getSymbol());
        assertEquals(0,robot.getX());
        assertEquals(0,robot.getY());
    }
    @Test
    public void SholudDownOver(){
        Robot robot = new Robot("Robot", 'R',0,Robot.MAX_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.MAX_Y, robot.getY());

    }
    @Test
    public void SholudUpNegative(){
        Robot robot = new Robot("Robot", 'R',0,Robot.MIN_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.MIN_Y, robot.getY());

    }

    @Test
    public void SholudDownSuccess(){
        Robot robot = new Robot("Robot", 'R',0,0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());

    }
    @Test
    public void SholudDownNSuccess1(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(true, robot.down(3));
        assertEquals(14, robot.getY());

    }
    @Test
    public void SholudDownNSuccess2(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(true, robot.down(5));
        assertEquals(16, robot.getY());

    }

    @Test
    public void SholudDownNFail1(){
        Robot robot = new Robot("Robot", 'R',10,5);
        assertEquals(false, robot.down(15));
        assertEquals(19, robot.getY());

    }

    @Test
    public void SholudDownNFail2(){
        Robot robot = new Robot("Robot", 'R',10,5);
        assertEquals(false, robot.down(18));
        assertEquals(19, robot.getY());

    }
    @Test
    public void SholudUpSuccess(){
        Robot robot = new Robot("Robot", 'R',0,1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());

    }

    @Test
    public void SholudUpNSuccess1(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(true, robot.up(5));
        assertEquals(6, robot.getY());
    }
    @Test
    public void SholudUpNSuccess2(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(true, robot.up(11));
        assertEquals(0, robot.getY());
    }

    @Test
    public void SholudUpNFail1(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(false, robot.up(12));
        assertEquals(0, robot.getY());
    }

    @Test
    public void SholudUpNFail2(){
        Robot robot = new Robot("Robot", 'R',10,11);
        assertEquals(false, robot.up(13));
        assertEquals(0, robot.getY());
    }

    @Test
    public void SholudleftNegative(){
        Robot robot = new Robot("Robot", 'R',Robot.MIN_X,1);
        assertEquals(false, robot.left());
        assertEquals(Robot.MIN_X, robot.getX());

    }
    
    @Test
    public void SholudRightOver(){
        Robot robot = new Robot("Robot", 'R',Robot.MAX_X,1);
        assertEquals(false, robot.right());
        assertEquals(Robot.MAX_X, robot.getX());

    }

    @Test
    public void SholudleftSuccess(){
        Robot robot = new Robot("Robot", 'R',1,1);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());

    }

    @Test
    public void SholudleftNSuccess1(){
        Robot robot = new Robot("Robot", 'R',6,1);
        assertEquals(true, robot.left(5));
        assertEquals(1, robot.getX());

    }
    @Test
    public void SholudleftNSuccess2(){
        Robot robot = new Robot("Robot", 'R',10,1);
        assertEquals(true, robot.left(5));
        assertEquals(5, robot.getX());

    }

    @Test
    public void SholudleftNFail1(){
        Robot robot = new Robot("Robot", 'R',10,1);
        assertEquals(false, robot.left(11));
        assertEquals(0, robot.getX());

    }
    @Test
    public void SholudleftNFail2(){
        Robot robot = new Robot("Robot", 'R',5,1);
        assertEquals(false, robot.left(8));
        assertEquals(0, robot.getX());

    }


    @Test
    public void SholudRightSuccess(){
        Robot robot = new Robot("Robot", 'R',1,1);
        assertEquals(true, robot.right());
        assertEquals(2, robot.getX());

    }
    @Test
    public void SholudRightNSuccess1(){
        Robot robot = new Robot("Robot", 'R',1,1);
        assertEquals(true, robot.right(6));
        assertEquals(7, robot.getX());

    }
    @Test
    public void SholudRightNSuccess2(){
        Robot robot = new Robot("Robot", 'R',5,1);
        assertEquals(true, robot.right(8));
        assertEquals(13, robot.getX());

    }

    @Test
    public void SholudRightNFail1(){
        Robot robot = new Robot("Robot", 'R',17,1);
        assertEquals(false, robot.right(8));
        assertEquals(19, robot.getX());

    }
    @Test
    public void SholudRightNFail2(){
        Robot robot = new Robot("Robot", 'R',19,1);
        assertEquals(false, robot.right(1));
        assertEquals(19, robot.getX());

    }

    

    
    
}
