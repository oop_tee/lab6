package com.pattarapol.lab6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BookBank pattarapol = new BookBank("Pattarapol",50.0);
        pattarapol.print();
        BookBank prayud = new BookBank("Prayuddd",10000.0);
        prayud.print();
        prayud.withdraw(4000.0);
        prayud.print();
        
        pattarapol.deposit(4000.0);
        pattarapol.print();

        BookBank prawit = new BookBank("Prawit",100000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();

    }
}
